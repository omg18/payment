#Build from base image is java 11
FROM openjdk:11

# Get the path of file .jar in local directory
ARG JAR_FILE=target/payment*.jar

# Create a working dir in docker
WORKDIR /opt/app

# Set volume point to /tmp
VOLUME /tmp

# Copy file .jar from local to the working dir in docker
# and rename file is user-service.jar
COPY ${JAR_FILE} payment.jar

# define the command default. After container start, these commands are executed.
ENTRYPOINT ["java","-jar","payment.jar"]

EXPOSE 10272
