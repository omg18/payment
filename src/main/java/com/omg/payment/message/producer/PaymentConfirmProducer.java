package com.omg.payment.message.producer;

import com.omg.payment.message.BaseProducerService;
import com.omg.payment.model.dto.ConfirmOrderDTO;
import com.omg.payment.model.dto.ConfirmPaymentDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PaymentConfirmProducer extends BaseProducerService<ConfirmPaymentDTO> {

    @Value(value = "${spring.kafka.topic.confirm-payment}")
    private String topicName;

    @Autowired
    public PaymentConfirmProducer(KafkaTemplate<String, ConfirmPaymentDTO> kafkaTemplate) {
        super(kafkaTemplate);
    }

    @Override
    protected String getTopicName() {
        return this.topicName;
    }
}
