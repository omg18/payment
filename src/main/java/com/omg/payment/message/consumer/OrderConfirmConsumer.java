package com.omg.payment.message.consumer;

import com.omg.payment.constant.ChannelPayment;
import com.omg.payment.constant.OrderStatus;
import com.omg.payment.constant.PaymentStatus;
import com.omg.payment.constant.UUIDPrefixEnum;
import com.omg.payment.entity.Payment;
import com.omg.payment.message.BaseConsumerService;
import com.omg.payment.model.dto.ConfirmOrderDTO;
import com.omg.payment.repository.PaymentRepository;
import com.omg.payment.utils.AppUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class OrderConfirmConsumer implements BaseConsumerService<ConfirmOrderDTO> {

    @Autowired
    private PaymentRepository paymentRepository;

    @KafkaListener(
            topics = "${spring.kafka.topic.confirm-order}"
            ,groupId = "${spring.kafka.consumer.group-id}"
    )
    @Override
    public void receive(ConfirmOrderDTO data) {

        // Check message data
        if (data == null) {
            log.info(" The message is null");
            return;
        }

        // Check order status
        if (!OrderStatus.CONFIRM.getValue().equals(data.getStatus())) {
            log.info(" The order status is not the confirm.");
            return;
        }

        // Check order info
        Optional<Payment> payment = this.paymentRepository.getPaymentInfo(data.getCustomerId(), data.getOrderCode(), data.getBillCode());
        if (!payment.isEmpty()) {
            log.info(" The order was confirmed payment.");
            return;
        }

        // Save payment info into DB
        this.paymentRepository.save(
                Payment.builder()
                        .id(AppUtils.generateUUID(UUIDPrefixEnum.PAYMENT.getValue()))
                        .orderCode(data.getOrderCode())
                        .billCode(data.getBillCode())
                        .customerId(data.getCustomerId())
                        .channelPaymentCode(data.getChannelPayment())
                        .status(PaymentStatus.UNPAID.getValue())
                        .build()
        );
    }
}
