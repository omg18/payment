package com.omg.payment.message.consumer;

import com.omg.payment.constant.OrderStatus;
import com.omg.payment.constant.PaymentStatus;
import com.omg.payment.entity.Payment;
import com.omg.payment.message.BaseConsumerService;
import com.omg.payment.model.dto.CancelOrderDTO;
import com.omg.payment.repository.PaymentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class OrderCancelConsumer implements BaseConsumerService<CancelOrderDTO> {

    @Autowired
    private PaymentRepository paymentRepository;

    @KafkaListener(
            topics = "${spring.kafka.topic.cancel-order}"
            ,groupId = "${spring.kafka.consumer.group-id}"
    )
    @Override
    public void receive(CancelOrderDTO data) {

        // Check message data
        if (data == null) {
            log.info(" The message is null");
            return;
        }

        // Check order status
        if (!OrderStatus.CANCEL.getValue().equals(data.getStatus())) {
            log.info(" The order status is not the confirm.");
            return;
        }

        // get payment info
        Optional<Payment> payment = this.paymentRepository.getPaymentInfo(data.getCustomerId(), data.getOrderCode(), data.getBillCode());
        if (payment.isEmpty()) {
            log.info(" The payment info is not exist.");
            return;
        }

        // Cancel payment info
        Payment paymentData = payment.get();
        paymentData.setStatus(PaymentStatus.CANCEL.getValue());
        this.paymentRepository.save(paymentData);
    }
}
