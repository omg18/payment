package com.omg.payment.message;

public interface BaseConsumerService<T>{

    void receive(T data);
}
