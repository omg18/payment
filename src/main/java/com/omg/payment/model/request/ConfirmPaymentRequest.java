package com.omg.payment.model.request;


import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConfirmPaymentRequest {

    @NotNull
    private Long customerId;

    @NotBlank
    private String orderCode;

    @NotBlank
    private String billCode;

    @NotBlank
    private String channelPayment;

}
