package com.omg.payment.model.dto;


import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CancelOrderDTO {

    private Long customerId;

    private String status;

    private String orderCode;

    private String billCode;
}
