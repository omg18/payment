package com.omg.payment.model.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConfirmPaymentDTO {

    private Long customerId;

    private String orderCode;

    private String billCode;
}
