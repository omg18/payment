package com.omg.payment.model.dto;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConfirmOrderDTO {

    private Long customerId;

    private String status;

    private String orderCode;

    private String billCode;

    private String channelPayment;
}
