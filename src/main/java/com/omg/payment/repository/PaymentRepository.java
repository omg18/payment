package com.omg.payment.repository;

import com.omg.payment.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author cuongnh
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, String> {

    @Query(value = "SELECT * FROM payment p " +
            "WHERE p.customer_id = :customerId " +
            "AND p.order_code = :orderCode " +
            "AND p.bill_code = :billCode", nativeQuery = true)
    Optional<Payment> getPaymentInfo(@Param("customerId") Long customerId,
                                     @Param("orderCode")String orderCode,
                                     @Param("billCode")String billCode);
}
