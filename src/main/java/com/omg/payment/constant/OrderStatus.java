package com.omg.payment.constant;

public enum OrderStatus {

    CREATE("CREATE"),
    CONFIRM("CONFIRM"),
    UNPAID("UNPAID"),
    PAID("PAID"),
    CANCEL("CANCEL");

    private final String value;

    OrderStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
