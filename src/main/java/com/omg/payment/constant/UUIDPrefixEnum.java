package com.omg.payment.constant;

/**
 * @author hieudn
 * This class defines all enum of UUID prefix.
 */
public enum UUIDPrefixEnum {

    PAYMENT("PAYMENT_");

    private final String value;

    UUIDPrefixEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
