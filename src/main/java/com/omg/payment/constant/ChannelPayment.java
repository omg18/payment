package com.omg.payment.constant;

public enum ChannelPayment {

    CASH("CASH"),
    CREDIT_CARD("CREDIT_CARD"),
    ATM("ATM");

    private final String value;

    ChannelPayment(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
