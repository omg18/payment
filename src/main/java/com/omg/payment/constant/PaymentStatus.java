package com.omg.payment.constant;

public enum PaymentStatus {

    UNPAID("UNPAID"),
    PAID("PAID"),
    CANCEL("CANCEL");

    private final String value;

    PaymentStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
