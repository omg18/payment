package com.omg.payment.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author luongtt
 *
 * Define the packages for repository
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.omg.payment.repository")
public class PersistenceJPAConfig {
}
