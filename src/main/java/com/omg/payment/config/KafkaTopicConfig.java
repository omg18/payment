package com.omg.payment.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Value("${spring.kafka.topic.confirm-order}")
    private String confirmOrderTopicName;

    @Value("${spring.kafka.topic.confirm-payment}")
    private String confirmPaymentTopicName;

    @Value("${spring.kafka.topic.cancel-order}")
    private String cancelOrderTopicName;

    @Bean
    public NewTopic confirmOrderTopic() {
        return TopicBuilder.name(this.confirmOrderTopicName).build();
    }

    @Bean
    public NewTopic confirmPaymentTopic() {
        return TopicBuilder.name(this.confirmPaymentTopicName).build();
    }

    @Bean
    public NewTopic cancelOrderTopic() {
        return TopicBuilder.name(this.cancelOrderTopicName).build();
    }
}
