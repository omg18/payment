package com.omg.payment.exception;

/**
 * @author cuongnh
 * The custom exception to handle the business exception
 */
public class PaymentBusinessException extends Exception{

    public PaymentBusinessException(String errorMessage){
        super(errorMessage);
    }
}
