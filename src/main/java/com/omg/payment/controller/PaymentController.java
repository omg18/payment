package com.omg.payment.controller;

import com.omg.payment.exception.PaymentBusinessException;
import com.omg.payment.model.request.ConfirmPaymentRequest;
import com.omg.payment.service.IPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/payment")
public class PaymentController {

    @Autowired
    private IPaymentService paymentService;

    @PutMapping("/confirm-payment")
    public void confirmPayment(@RequestBody @Valid ConfirmPaymentRequest request) throws PaymentBusinessException {
        this.paymentService.confirmPayment(request);
    }
}
