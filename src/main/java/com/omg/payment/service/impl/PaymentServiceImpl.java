package com.omg.payment.service.impl;

import com.omg.payment.constant.PaymentStatus;
import com.omg.payment.entity.Payment;
import com.omg.payment.exception.PaymentBusinessException;
import com.omg.payment.message.producer.PaymentConfirmProducer;
import com.omg.payment.model.dto.ConfirmPaymentDTO;
import com.omg.payment.model.request.ConfirmPaymentRequest;
import com.omg.payment.repository.PaymentRepository;
import com.omg.payment.service.IPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PaymentServiceImpl implements IPaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentConfirmProducer paymentConfirmProducer;

    @Override
    public void confirmPayment(ConfirmPaymentRequest request) throws PaymentBusinessException {

        // Get the payment info from DB and check exist
        Optional<Payment> payment =
                this.paymentRepository.getPaymentInfo(request.getCustomerId(), request.getOrderCode(), request.getBillCode());
        if(payment.isEmpty()) {
            throw new PaymentBusinessException("The payment info does not exist.");
        }

        // Check status of payment info
        Payment paymentData = payment.get();
        if ( PaymentStatus.PAID.getValue().equals(paymentData.getStatus())
                || PaymentStatus.CANCEL.getValue().equals(paymentData.getStatus())) {
            throw new PaymentBusinessException("The payment status is invalid.");
        }

        // Update status of payment info
        paymentData.setStatus(PaymentStatus.PAID.getValue());
        this.paymentRepository.save(paymentData);

        // Send the payment info to trade service (using kafka message)
        this.paymentConfirmProducer.send(ConfirmPaymentDTO.builder()
                .customerId(paymentData.getCustomerId())
                .orderCode(paymentData.getOrderCode())
                .billCode(paymentData.getBillCode())
                .build());
    }
}
