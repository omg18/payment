package com.omg.payment.service;

import com.omg.payment.exception.PaymentBusinessException;
import com.omg.payment.model.request.ConfirmPaymentRequest;

public interface IPaymentService {

    void confirmPayment(ConfirmPaymentRequest request) throws PaymentBusinessException;
}
